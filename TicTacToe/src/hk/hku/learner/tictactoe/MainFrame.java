package hk.hku.learner.tictactoe;

import javax.swing.JFrame;

public class MainFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2085694532054905274L;
	
	public MainFrame() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(500, 500);
		setTitle("TicTacToe by Tommy Li :D");
		
		add(new MainPanel());		
		
		setVisible(true);
	}

}
