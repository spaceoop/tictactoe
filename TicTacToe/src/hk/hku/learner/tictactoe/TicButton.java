package hk.hku.learner.tictactoe;

import java.io.Serializable;

import javax.swing.JButton;

public class TicButton extends JButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3313035847821840686L;
	
	private State buttonState;
	
	public TicButton() {
		setState(State.EMPTY);
	}
	
	public State getState() {
		return buttonState;
	}
	
	public void setState(State newState) {
		buttonState = newState;
		setText(buttonState.getDisplay());
		setFont(getFont().deriveFont(20.0f));
	}
	
	public enum State implements Serializable {
		
		FIRST("X"),
		SECOND("O"),
		EMPTY(" ");
		
		private String dispString;
		
		private State(String display) {
			dispString = display;
		}
		
		public String getDisplay() {
			return dispString;
		}
	}

}
