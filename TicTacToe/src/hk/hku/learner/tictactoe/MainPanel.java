package hk.hku.learner.tictactoe;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class MainPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7235334258712317695L;

	private int turn;
	private JLabel turnLabel;
	private ButtonPanel bp;
	private LogFrame logger;

	public MainPanel() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		turn = 0;
		logger = null;

		JLabel titleLabel = new JLabel("TicTacToe v0.1", JLabel.CENTER);
		titleLabel.setFont(titleLabel.getFont().deriveFont(30.0f));
		titleLabel.setAlignmentX(CENTER_ALIGNMENT);
		add(titleLabel);

		turnLabel = new JLabel("Turn now: X", JLabel.CENTER);
		turnLabel.setFont(titleLabel.getFont().deriveFont(20.0f));
		turnLabel.setAlignmentX(CENTER_ALIGNMENT);
		add(turnLabel);

		JPanel littlePanel = new JPanel(new FlowLayout());

		JButton logButton = new JButton("Show Log");
		logButton.setAlignmentX(CENTER_ALIGNMENT);
		logButton.addActionListener((event) -> {
			logger = new LogFrame();
		});
		littlePanel.add(logButton);

		JButton loadButton = new JButton("Load Board");
		loadButton.addActionListener((event) -> bp.loadState());
		littlePanel.add(loadButton);

		JButton saveButton = new JButton("Save Board");
		saveButton.addActionListener((event) -> bp.saveState());
		littlePanel.add(saveButton);

		add(littlePanel);

		bp = new ButtonPanel();
		bp.setAlignmentX(CENTER_ALIGNMENT);
		add(bp);

	}

	public void updateTurn() {
		turn++;
		TicButton.State win = bp.getWinner();
		if (win == TicButton.State.FIRST) {
			turnLabel.setText("X Wins!!!");
			if (logger != null)
				logger.logMessage("Game over! X wins.");
		} else if (win == TicButton.State.SECOND) {
			turnLabel.setText("O Wins!!!");
			if (logger != null)
				logger.logMessage("Game over! O wins.");
		} else if (turn == 9) {
			turnLabel.setText("Draw :C");
			if (logger != null)
				logger.logMessage("Game over. No one wins.");
		} else if (turn % 2 == 0) {
			turnLabel.setText("Turn now: X");
			if (logger != null)
				logger.logMessage("It is the turn of X.");
		} else {
			turnLabel.setText("Turn now: O");
			if (logger != null)
				logger.logMessage("It is the turn of O.");
		}
	}

	private class ButtonPanel extends JPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1918388543896477338L;

		private TicButton[][] tba;

		public ButtonPanel() {
			tba = new TicButton[3][3];

			setLayout(new GridBagLayout());
			setBorder(new EmptyBorder(5, 5, 5, 5));

			GridBagConstraints gbc = new GridBagConstraints();

			gbc.fill = GridBagConstraints.BOTH;
			gbc.insets = new Insets(5, 5, 5, 5);
			gbc.weightx = gbc.weighty = 1.0;

			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 3; j++) {
					tba[i][j] = new TicButton();
					gbc.gridx = i;
					gbc.gridy = j;

					tba[i][j].addActionListener((event) -> {
						TicButton tb = (TicButton) event.getSource();
						if (getWinner() != TicButton.State.EMPTY || turn == 9) {
							int op = JOptionPane.showConfirmDialog(null,
									"The game is over.\nWould you like to reset the board?", "Game Over!",
									JOptionPane.YES_NO_OPTION);
							if (op == JOptionPane.YES_OPTION) {
								for (int x = 0; x < 3; x++)
									for (int y = 0; y < 3; y++)
										tba[x][y].setState(TicButton.State.EMPTY);
								turn = 0;
								if (logger != null)
									logger.logMessage("Board have been successfully reset.");
								turnLabel.setText("Turn now: X");
								if (logger != null)
									logger.logMessage("It is the turn of X.");
							}
						} else if (tb.getState() != TicButton.State.EMPTY) {
							JOptionPane.showMessageDialog(null, "Can't choose non-empty block", "Error!",
									JOptionPane.WARNING_MESSAGE);
						} else {
							if (turn % 2 == 0) {
								tb.setState(TicButton.State.FIRST);
								if (logger != null)
									logger.logMessage("Player X made his turn.");
							} else {
								tb.setState(TicButton.State.SECOND);
								if (logger != null)
									logger.logMessage("Player O made his turn.");
							}
							updateTurn();
						}
					});

					add(tba[i][j], gbc);
				}

		}

		public void loadState() {
			File fi = new File("boardstate.dat");
			if (!fi.exists()) {
				try {
					if (logger != null)
						logger.logMessage("boardstate.dat not found, creating a new one.");
					fi.createNewFile();
				} catch (IOException e) {
					if (logger != null)
						logger.logMessage("ERROR: File creation failed :C");
				}
			}
			try {
				FileInputStream fis = new FileInputStream(fi);
				ObjectInputStream ois = new ObjectInputStream(fis);
				for (int i = 0; i < 3; i++)
					for (int j = 0; j < 3; j++)
						tba[i][j].setState((TicButton.State) ois.readObject());
				turn = (Integer) ois.readObject() - 1;
				if (logger != null)
					logger.logMessage("Successfully loaded board from file boardstate.dat.");
				updateTurn();
				ois.close();
			} catch (FileNotFoundException e) {
				if (logger != null)
					logger.logMessage("ERROR: File not found :C");
			} catch (IOException e) {
				if (logger != null)
					logger.logMessage("ERROR: Error when setting up output stream :C");
			} catch (ClassNotFoundException e) {
				if (logger != null)
					logger.logMessage("ERROR: Error when reading output stream :C");
			}

		}

		public void saveState() {
			File fi = new File("boardstate.dat");
			if (!fi.exists()) {
				try {
					if (logger != null)
						logger.logMessage("boardstate.dat not found, creating a new one.");
					fi.createNewFile();
				} catch (IOException e) {
					if (logger != null)
						logger.logMessage("ERROR: File creation failed :C");
				}
			}
			try {
				FileOutputStream fos = new FileOutputStream(fi);
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				for (int i = 0; i < 3; i++)
					for (int j = 0; j < 3; j++)
						oos.writeObject(tba[i][j].getState());
				oos.writeObject(Integer.valueOf(turn));
				if (logger != null)
					logger.logMessage("Successfully saved board to file boardstate.dat.");
				oos.close();
			} catch (FileNotFoundException e) {
				if (logger != null)
					logger.logMessage("ERROR: File not found :C");
			} catch (IOException e) {
				if (logger != null)
					logger.logMessage("ERROR: Error when setting up output stream :C");
			}
		}

		public TicButton.State getWinner() {
			for (int i = 0; i < 3; i++)
				if (tba[i][0].getState() == tba[i][1].getState() && tba[i][0].getState() == tba[i][2].getState()
						&& tba[i][0].getState() != TicButton.State.EMPTY) {
					return tba[i][0].getState();
				}
			for (int i = 0; i < 3; i++)
				if (tba[0][i].getState() == tba[1][i].getState() && tba[1][i].getState() == tba[2][i].getState()
						&& tba[0][i].getState() != TicButton.State.EMPTY) {
					return tba[0][i].getState();
				}
			if (tba[0][0].getState() == tba[1][1].getState() && tba[1][1].getState() == tba[2][2].getState()
					&& tba[0][0].getState() != TicButton.State.EMPTY)
				return tba[0][0].getState();
			if (tba[2][0].getState() == tba[1][1].getState() && tba[1][1].getState() == tba[0][2].getState()
					&& tba[2][0].getState() != TicButton.State.EMPTY)
				return tba[2][0].getState();
			return TicButton.State.EMPTY;
		}

	}

}
