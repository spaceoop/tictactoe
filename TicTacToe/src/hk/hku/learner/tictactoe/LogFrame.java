package hk.hku.learner.tictactoe;

import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class LogFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5249730867029940330L;
	
	private JTextArea log;
		
	public LogFrame() {
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setSize(600, 300);
		setTitle("TicTacToe Log");
		
		log = new JTextArea();
		
		add(new JScrollPane(log));		
		
		setVisible(true);
	}
	
	public void logMessage(String msg) {
		String logMsg = "[" + new Date().toString() + "] " + msg + "\n";
		log.append(logMsg);
	}

}
